use Time

select * from Times;

----Procidore Excluir

create procedure ExcluirTimePorId
	@TimeId int 
as
	begin 
		Delete from dbo.Times where TimeId = @TimeId
	end

exec ExcluirTimePorId @TimeId = 2

----Procidore Atualizar

create procedure AtualizarTime
(
	@TimeId int ,
	@Time nvarchar(100),
	@Estado char(2),
	@Cores varchar(50)
)
as
	begin 
		update dbo.Times set Time = @Time,
				Estado = @Estado,
				Cores = @Cores
			where TimeId = @TimeId
	end

exec AtualizarTime @TimeId = 1, 
		@Time = 'Fla',
		@Estado = 'BR',
		@Cores = 'Vermelho e Preto'



----Procidore Incluir

create procedure IncluirTime
(
	@Time nvarchar(100),
	@Estado char(2),
	@Cores varchar(50)
)
as
	begin 
		insert into dbo.Times values (@Time, @Estado, @Cores)
	end

exec IncluirTime  @Time = 'Fla',
		@Estado = 'BR',
		@Cores = 'Vermelho e Preto'


----Procidore Obter

create procedure ObterTimes

as
	begin 
		Select TimeId,Time, Estado, Cores from  dbo.Times
	end

exec ObterTimes