﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SistemaTime.Models
{
    public class Times
    {
        [Display(Name ="Id")]
        public int TimesId { get; set; }


        [Required(ErrorMessage ="Informe o nome do time")]
        public string Time { get; set; }

        [Required(ErrorMessage = "Informe o estado do time")]
        public string Estado { get; set; }

        [Required(ErrorMessage = "Informe as cores do time")]
        public string Cores { get; set; }
    }
}