﻿using SistemaTime.Models;
using SistemaTime.Repositorio;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SistemaTime.Controllers
{
    public class TimeController : Controller
    {

        private TimeRepositorio _repositorio;

        public ActionResult ObterTimes()
        {
            _repositorio = new TimeRepositorio();

            ModelState.Clear();
            return View(_repositorio.ObterTimes());

        }

        [HttpGet]
        public ActionResult IncluirTime()
        {
            return View();
        }


        [HttpPost]
        public ActionResult IncluirTime(Times timeObj)
        {
            try{
                if(ModelState.IsValid)
                {
                    _repositorio = new TimeRepositorio();
                    if (_repositorio.AdicionarTime(timeObj))
                    {
                        ViewBag.Mensagem = "Time cadastrado com sucesso";
                    }
                    
                }
                return View();
            }
            catch (Exception)
            {
                return View("ObterTimes");
            }
        }


        [HttpGet]
        public ActionResult EditarTime(int id)
        {
            _repositorio = new TimeRepositorio();
            return View(_repositorio.ObterTimes().Find(t => t.TimesId==id));
        }


        [HttpPost]
        public ActionResult EditarTime(int id , Times timeObj)
        {
            try
            {
                _repositorio = new TimeRepositorio();
                _repositorio.AtualizarTime(timeObj);
                return RedirectToAction("ObterTimes");

            }
            catch (Exception)
            {
                return View("ObterTimes");
            }
        }
    
        
        public ActionResult ExcluirTime(int id)
        {
            try
            {
                _repositorio = new TimeRepositorio();
                if (_repositorio.ExcluirTime(id))
                {
                    ViewBag.Mensagem = "Time excluido com sucesso";
                }
                return RedirectToAction("ObterTimes");
            }
            catch (Exception)
            {
                return View("ObterTimes");
            }
        }
    }
}